# Laravel Selectable
Select2 handler for laravel

### Quick Start
Let's assume that we have created a laravel project and will create a select2 handler for selecting users.

##### Model
Let's load Selectable module and use it in model
```
use Otls\LaravelSelectable\Selectable;

```

Add folowing properties
```
/**
* This is a mandatory property determining what columns will be selected
*
* @var array
*/
protected $selectable = [
    'id' => 'id',
    'text' => 'name'
];

/**
* This determines what columns will be used to run searching query.
* If this property is not defined, property $selectable will be used.
*
* @var array
*/
protected $searchable = [
    'username',
    'name'
];

/**
* This determines what columns will be filters.
* If this property is not defined, property $selectable will be used.
*
* @var array
*/
protected $filterable = [
    'username',
    'id'
];

/**
* This determines what columns will be filters.
* Default 5.
*
* @var int
*/
protected $selectableLimit = 5;
```
##### Usage
```
// same one
$data = User::options("search someone");
$data = User::selectable("search someone");

// same one
$data = User::whereDay("created_at", "<", date('d'))->options("search");
$data = User::whereDay("created_at", "<", date('d'))->selectable("search");

// using filter
// filters that will be used only as what has been defined in property $filterable of the model
$districts = District::options("search district", ['regency_id' => 112]);
```

##### Result
The result will be instance of `\Illuminate\Contracts\Pagination\LengthAwarePaginator` which could directly be returned as a response
```
{
  "current_page": 1,
  "data": [
    {
      "id": 12,
      "text": "Mrs. Effie Kilback"
    },
    {
      "id": 13,
      "text": "Prof. Addison Cole"
    },
    {
      "id": 14,
      "text": "Nya Fahey"
    },
    {
      "id": 15,
      "text": "Dr. Kyler Adams Sr."
    },
    {
      "id": 16,
      "text": "Carson Orn Jr."
    }
  ],
  "first_page_url": "http://larapack.test/s2?page=1",
  "from": 1,
  "last_page": 22,
  "last_page_url": "http://larapack.test/s2?page=22",
  "links": [
    {
      "url": null,
      "label": "&laquo; Previous",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=1",
      "label": "1",
      "active": true
    },
    {
      "url": "http://larapack.test/s2?page=2",
      "label": "2",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=3",
      "label": "3",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=4",
      "label": "4",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=5",
      "label": "5",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=6",
      "label": "6",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=7",
      "label": "7",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=8",
      "label": "8",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=9",
      "label": "9",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=10",
      "label": "10",
      "active": false
    },
    {
      "url": null,
      "label": "...",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=21",
      "label": "21",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=22",
      "label": "22",
      "active": false
    },
    {
      "url": "http://larapack.test/s2?page=2",
      "label": "Next &raquo;",
      "active": false
    }
  ],
  "next_page_url": "http://larapack.test/s2?page=2",
  "path": "http://larapack.test/s2",
  "per_page": 5,
  "prev_page_url": null,
  "to": 5,
  "total": 109
}
```
