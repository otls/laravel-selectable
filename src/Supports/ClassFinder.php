<?php

namespace Otls\LaravelSelectable\Supports;

use Illuminate\Support\Facades\Cache;
use Otls\LaravelSelectable\Contracts\SelectableSource;
use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ReflectionClass;

/**
 * handler for finding classes
 */
trait ClassFinder
{
    /**
     * collect classes in a directory
     *
     * @param string $path
     * @return array
     */
    public function collectClasses(string $path): array
    {
        $classes = [];
        $Directory = new RecursiveDirectoryIterator($path);
        $Iterator = new RecursiveIteratorIterator($Directory);

        foreach ($Iterator as $fullpath => $info) {
            $file = $info->getPathname();
            if ($this->isPHP($file) && $class = $this->getName($file)) {

                $classArray = explode("\\", $class);
                $className = end($classArray);

                $classes[$className] = [
                    'class' => $class,
                    'className' => $className,
                    'filename' => $fullpath
                ];
            }
        }
        return $classes;
    }

    /**
     * Resolve a class by given name and config.key
     *
     * @param string $name
     * @param string $config
     * @return object|string|bool
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function resolveByConfig(string $name, string $config = 'selectable.sources')
    {
        $classes = Cache::store('file')->get($config);
        $name = ucfirst($name);
        $class = $classes[$name] ?? null;

        if ($class && class_exists($class['class'])) {
            return $class['class'];
        }

        return false;
    }

    /**
     * check if php file
     *
     * @param string $file
     * @return bool
     */
    public function isPHP(string $file) :bool
    {
        return strpos($file, '.php');
    }

    /**
     * Get name from filename
     *
     * @param string $file
     * @return string|bool
     */
    public function getName(string $file)
    {
        $name = "App";
        $name .= str_replace([app_path(), ".php", "/"], "", $file);

        $reflection = new ReflectionClass($name);
        $ifaces = $reflection->getInterfaceNames();

        if (in_array('Otls\LaravelSelectable\Contracts\SelectableSource', $ifaces)) {
            return $name;
        } else {
            return false;
        }
    }

    public function getNameIfPHPAndChildOf(string $file, string $iface)
    {
        if ($this->isPHP($file)) {
            $name = $this->getName($file);
            if (is_subclass_of($name, $iface, true)) {
                return $name;
            }
        }
        return false;
    }
}
