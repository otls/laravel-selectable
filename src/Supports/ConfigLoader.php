<?php

namespace Otls\LaravelSelectable\Supports;
/**
 * Load config
 */
trait ConfigLoader
{
    public function loadConfig()
    {
        return config('selectable') ?? include(__DIR__ . "/../Configs/selectable.php");
    }
}
