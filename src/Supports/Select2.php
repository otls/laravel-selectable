<?php

namespace Otls\LaravelSelectable\Supports;

use Exception;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

/**
 * Select2 handlers / SelectableSource implementation
 */
trait Select2
{
    /**
     * Filterable property name that the parent should have
     * This determines what columns will be filters.
     *
     * @var string
     */
    private $filterableProp = 'filterable';

    /**
     * Searchable property name that the parent should have
     * This determines what columns will be used to run searching query.
     *
     * @var string
     */
    private $searchableProp = 'searchable';

    /**
     * Selectable property name that the parent must have.
     * This is a mandatory property determining what columns will be selected
     *
     * @var string
     */
    private $selectableProp = 'selectable';

    /**
     * Data limitation property that the parent should have.
     * Determining data pagination.
     *
     * @var string
     */
    private $limitProp = 'selectableLimit';

    /**
     * Get the options
     *
     * @param string $search
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getSelectable(string $search = null, array $filters = []): LengthAwarePaginator
    {
        $src = $this->selectQuery() ?? $this;
        return $this->executeQuery($src, $search, $filters);
    }

    /**
     * Selectable scope for model which is using Selectable
     *
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model $q
     * @param string $search
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function scopeSelectable($q, string $search = null, array $filters = []): LengthAwarePaginator
    {
        return $this->executeQuery($q, $search, $filters);
    }

    /**
     * Aliasing scopeSelectable
     *
     * @param \Illuminate\Database\Eloquent\Builder $q
     * @param string $search
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function scopeOptions(Builder $q, string $search = null, array $filters = []): LengthAwarePaginator
    {
        return $this->scopeSelectable($q, $search, $filters);
    }

    /**
     * Aliasing GetSelectable
     *
     * @param string $search
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getOptions(string $search = null, array $filters = []): LengthAwarePaginator
    {
        return $this->getSelectable($search, $filters);
    }

    /**
     * Get select option
     *
     * @param array $filters
     * @return null|\Illuminate\Database\Eloquent\Model
     */
    public function getSelectedData(array $filters)
    {
        $src = $this->selectQuery() ?? $this;
        $filters = $this->onlyFilterable($filters);
        return $src
            ->filter($src, $filters)
            ->selectRaw($this->selectAttributes())
            ->first();
    }

    /**
     * Get property needed by Select2Able
     *
     * @param string $property
     * @return mixed
     * @throws \Exception;
     */
    public function getProperty(string $property)
    {
        if (property_exists($this, $property)) {
            return $this->{$property};
        }

        throw new Exception("No property \${$property}, please define it.");
    }

    /**
     * Make query builder that will be used to get data options
     *
     * @return \Illuminate\Database\Eloquent\Builder|null
     */
    public function selectQuery(): ?Builder
    {
        return null;
    }

    /**
     * Execute the query
     *
     * @param \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder $src
     * @param string $search
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function executeQuery(
        $src,
        string $search = null,
        array $filters = []
        ): LengthAwarePaginator
    {
        $selectable = $this->getProperty($this->selectableProp);
        $searchable = $this->{$this->searchableProp} ?? $selectable;
        $filters = $this->onlyFilterable($filters);

        $data = $src
            ->when(!empty($filters), function ($q) use ($filters) {
                return $this->filter($q, $filters);
            })
            ->when($search, function ($q) use ($search, $searchable) {
                return $this->search($q, $search, $searchable);
            })
            ->selectRaw($this->selectAttributes())
            ->toBase()
            ->paginate($this->{$this->limitProp} ?? 5);

        return $data;
    }

    /**
     * Select only data needed
     *
     * @return string
     */
    public function selectAttributes()
    {
        $selectable = $this->getProperty($this->selectableProp);
        return "{$selectable['id']} AS id, {$selectable['text']} AS text";
    }

    /**
     * Filter data according to filters property
     *
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model $q
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function filter($q, array $filters)
    {
        foreach ($filters as $col => $val) {
            $q->where($col, $val);
        }
        return $q;
    }

    /**
     * Search data according to searchable property
     *
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model $q
     * @param string $search
     * @param array $searchable
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function search($q, string $search, array $searchable)
    {
        $first = true;
        foreach ($searchable as $k => $column) {
            if (!$first) {
                $q->orWhere($column, "LIKE", "%{$search}%");
            } else {
                $q->where($column, "LIKE", "%{$search}%");
                $first = false;
            }
        }
        return $q;
    }

    /**
     * Only get filterable columns
     *
     * @param array $filters
     * @return array $filters
     */
    public function onlyFilterable(array $filters = []): array
    {
        if ($filters) {
            $filterable = $this->{$this->filterableProp} ?? $this->getProperty($this->selectableProp);
            $filters = array_intersect_key($filters, array_flip($filterable));
        }
        return $filters;
    }
}
