<?php

namespace Otls\LaravelSelectable\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Otls\LaravelSelectable\Listeners\ModelCreated;
use Otls\LaravelSelectable\Modules\Resolver;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Console\Events\CommandFinished;
use Otls\LaravelSelectable\Commands\SelectableRefresh;
use oTLS\LaravelSelectable\Contracts\Selectable;
use Otls\LaravelSelectable\Services\SelectableService;

class LaravelSelectableProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->call([$this, 'registerSolver']);
        $this->app->call([$this, 'modelListener']);

        $this->commands([
            SelectableRefresh::class
        ]);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../Configs/selectable.php' => config_path('selectable.php'),
        ], 'otls-selectable');

        $this->resolver = new Resolver;
        $this->resolver->setConfig();
        $this->loadRoutesFrom(__DIR__ . '/../Routes/web.php');
    }

    public function registerSolver(\Illuminate\Http\Request $request)
    {
        return $this->app->bind(Selectable::class, function ($app) use ($request) {
            if ($request->selectable) {
                return (new SelectableService)->resolve($request->selectable);
            }
            throw new NotFoundHttpException;
        });
    }

    public function modelListener()
    {
        Event::listen(CommandFinished::class, ModelCreated::class);
    }
}
