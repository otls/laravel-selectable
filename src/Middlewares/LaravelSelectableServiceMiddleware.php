<?php

namespace Otls\LaravelSelectable\Middlewares;

use Closure;
use Illuminate\Http\Request;
use Otls\LaravelSelectable\Supports\ConfigLoader;

class LaravelSelectableServiceMiddleware
{
    use ConfigLoader;
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $configs = $this->loadConfig();
        if ($configs['enableService']) {
            return $next($request);
        }

        abort(404);
    }
}
