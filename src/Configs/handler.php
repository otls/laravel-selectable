<?php

use Otls\LaravelSelectable\Controllers\SelectableController;

return [
    'getOptions' => [SelectableController::class, 's2'],
    'getSelectedOption' => [SelectableController::class, 's2Selected']
];
