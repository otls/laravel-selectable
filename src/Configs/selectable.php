<?php

return [
    /**
     * App could handle select2 request automatically
     */
    'enableService' => true,
    /**
     * Prefix for selectable service routes
     */
    'prefix' => "/masters",
    /**
     *  set selectable route name as
     */
    'as' => 'masters',
    /**
     * applied to global selectable routes
     */
    'middlewares' => [],
    /**
     * slectable service routing
     */
    'routes' => [
        'getOptions' => [
            'path' => '/s2/{selectable}',
            'middlewares' => [],
            'name' => 's2'
        ],
        'getSelectedOption' => [
            'path' => '/s2/{selectable}/selected',
            'middlewares' => [],
            'name' => 's2.selected'
        ]
    ],

    /**
     * Refresh selectable source cache after these commands
     */
    'refreshAfterCommands' => [
        'make:model',
        'make:controller',
        'make:request',
        'make:migration',
        'config:cache',
        'optimize',
        'config:cache',
        'route:cache'
    ]
];
