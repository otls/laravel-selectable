<?php

namespace Otls\LaravelSelectable\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Artisan;
use Otls\LaravelSelectable\Modules\Resolver;
use Otls\LaravelSelectable\Supports\ClassFinder;
use Otls\LaravelSelectable\Supports\ConfigLoader;

class ModelCreated
{
    use ConfigLoader;
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle($event)
    {
        $configs = $this->loadConfig();
        if (in_array($event->command, $configs, true)) {
            $resolver = new Resolver;
            $resolver->setConfig();
        }
    }
}
