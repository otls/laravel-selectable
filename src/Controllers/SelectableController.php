<?php

namespace Otls\LaravelSelectable\Controllers;

use Illuminate\Http\Request;
use oTLS\LaravelSelectable\Contracts\Selectable;
use Otls\LaravelSelectable\Requests\SelectableRequest;
use Otls\LaravelSelectable\Requests\SelectedRequest;

class SelectableController extends Controller
{
    public function s2(SelectableRequest $request, Selectable $service)
    {
        return response()->json($service->s2($request));
    }

    public function s2Selected(SelectedRequest $request, Selectable $service)
    {
        return response()->json($service->s2Selected($request));
    }
}
