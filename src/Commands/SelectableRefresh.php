<?php

namespace Otls\LaravelSelectable\Commands;

use Illuminate\Console\Command;
use Otls\LaravelSelectable\Modules\Resolver;

class SelectableRefresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'selectable:refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Recache the sources';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $resolver = new Resolver;
        $resolver->setConfig();

        $this->info("Selectable source refreshed");
    }
}
