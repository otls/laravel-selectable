<?php

namespace Otls\LaravelSelectable\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

interface SelectableSource
{
    /**
     * Selectable scope for model which is using Selectable
     *
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model $q
     * @param string $search
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function scopeSelectable($q, string $search = null, array $filters = []);

    /**
     * Get property needed by Select2Able
     *
     * @param string $property
     * @return mixed
     * @throws \Exception;
     */
    public function getProperty(string $property);

    /**
     * Get query will be used to get data select-able
     *
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model|null
     */
    public function selectQuery();

    /**
     * Execute the query
     *
     * @param \Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Builder $src
     * @param string $search
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function executeQuery(
        $src,
        string $search = null,
        array $filters = []
        ): LengthAwarePaginator;

    /**
     * Filtering data
     *
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model $q
     * @param array $filters
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function filter($q, array $filters);

    /**
     * Searching data
     *
     * @param \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model $q
     * @param string $search
     * @param array $searchAble
     * @return \Illuminate\Database\Eloquent\Builder|\Illuminate\Database\Eloquent\Model
     */
    public function search($q, string $search, array $searchAble);

    /**
     * Only get filterable column
     *
     * @param array $filters
     * @return array $filters
     */
    public function onlyFilterable(array $filters = []): array;

    /**
     * Get select option
     *
     * @param array $filters
     * @return null|\Illuminate\Database\Eloquent\Model
     */
    public function getSelectedData(array $filters);
}
