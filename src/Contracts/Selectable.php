<?php

namespace oTLS\LaravelSelectable\Contracts;

use Otls\LaravelSelectable\Modules\Resolver;
use Otls\LaravelSelectable\Requests\SelectableRequest;
use Otls\LaravelSelectable\Requests\SelectedRequest;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

abstract class Selectable
{
    protected $source;
    /**
     * Getting options from model
     *
     * @param \Otls\LaravelSelectable\Requests\SelectableRequest $request
     */
    public function s2(SelectableRequest $request)
    {
        $search = $request->search;
        $filters = json_decode($request->filters ?? "[]", true);
        return $this->source->getSelectable($search, $filters);
    }

    public function s2Selected(SelectedRequest $request)
    {
        $filters = json_decode($request->filters ?? "[]", true);
        return $this->source->getSelectedData($filters);
    }

    /**
     * Resolve the source
     *
     * @param string $source
     * @return \Otls\LaravelSelectable\Contracts\SelectableSource
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     */
    public function resolve(string $source)
    {
        try {
            $resolver = new Resolver;
            $this->source = $resolver->resolve($source, true);
            return $this;
        } catch (NotFoundHttpException $th) {
            throw $th;
        }
    }
}
