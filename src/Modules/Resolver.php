<?php

namespace Otls\LaravelSelectable\Modules;

use Otls\LaravelSelectable\Supports\ClassFinder as SupportsClassFinder;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Illuminate\Support\Facades\Cache;

class Resolver
{
    use SupportsClassFinder;

    protected $modulePath = 'Models';
    protected $configName = "selectable";
    protected $sourceKey = "sources";

    public function setConfig(): void
    {
        $key = $this->getKey();
        Cache::store('file')->forget($key);
        // return;
        // dd($key);
        Cache::store('file')->rememberForever($key, function () {
            $path = app_path($this->modulePath);
            $classes = $this->collectClasses($path);
            return $classes;
        });
    }

    public function resolve(string $name, bool $toObject = false)
    {
        $key = $this->getKey();
        if ($class = $this->resolveByConfig($name, $key)) {
            return $toObject ? new $class() : $class;
        }
        throw new NotFoundHttpException();
    }

    public function getKey()
    {
        return "{$this->configName}.{$this->sourceKey}";
    }
}
