<?php

namespace Otls\LaravelSelectable\Modules;

use App\Http\Middleware\LaravelSelectableServiceMiddleware;
use Illuminate\Support\Facades\Route;
use Otls\LaravelSelectable\Controllers\SelectableController;
use Otls\LaravelSelectable\Supports\ConfigLoader;

class Router
{
    use ConfigLoader;
    /**
     * controllers
     *
     * @var array
     */
    protected $handlers = [
        'getOptions' => [SelectableController::class, 's2'],
        'getSelectedOption' => [SelectableController::class, 's2Selected']
    ];

    public function __construct() {
        $this->apply();
    }

    public function apply()
    {
        $config = $this->loadConfig();
        $routes = $config['routes'];

        return

        Route::prefix($config['prefix'])
        ->as($config['as'])
        ->middleware(LaravelSelectableServiceMiddleware::class)
        ->middleware($config['middlewares'])
        ->group(function () use ($routes) {
            foreach ($routes as $k => $route) {
                Route::get($route['path'], $this->handlers[$k])
                    ->middleware($route['middlewares'] ?? [])
                    ->name($route['name'] ?? null);
            }
        });
    }
}


