<?php

namespace Otls\LaravelSelectable;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Otls\LaravelSelectable\Supports\Select2;

/**
 * Aliasing Select2 handlers / SelectableSource implementation
 */
trait Selectable
{
    use Select2;

    /**
     * Aliasing scopeSelectable
     *
     * @param \Illuminate\Database\Eloquent\Builder $q
     * @param string $search
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function scopeOptions(Builder $q, string $search = null, array $filters = []): LengthAwarePaginator
    {
        return $this->scopeSelectable($q, $search, $filters);
    }

    /**
     * Aliasing GetSelectable
     *
     * @param string $search
     * @param array $filters
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getOptions(string $search = null, array $filters = []): LengthAwarePaginator
    {
        return $this->getSelectable($search, $filters);
    }
}
