<?php

use Illuminate\Support\Facades\Route;
use Otls\LaravelSelectable\Middlewares\LaravelSelectableServiceMiddleware;
use Otls\LaravelSelectable\Modules\Router;

Route::middleware(LaravelSelectableServiceMiddleware::class)->group(function () {
    new Router;
});
